<?php
    if (empty($_POST["username"]))
    {
        echo "No username recieved";
        exit;
    }
    else if (empty($_POST["password"]))
    {
        echo "No password received";
        exit;
    }

    include("dbconfig.php");

    $username = $_POST["username"];

    // Hashed passwords are stored in the database, so we need to hash it to compare
    $password = md5($_POST["password"]);

    // Debug to test hashing algorithm
    echo $password;

    if (empty($_COOKIE['jhplumbingauth']))
    {
        $loginQuery = $sqlCon->prepare("SELECT id,username FROM users WHERE username=:username AND password=:password LIMIT 1");
        $loginQuery->bindParam(':username', $_POST["username"]);
        $loginQuery->bindParam(':password', $_POST["password"]);

        if ($loginQuery->execute())
        {
            if ($loginQuery->rowCount() == 1)
            {
                if (session_id() == null) session_start();

                $queryResult = $loginQuery->fetch();
                $_SESSION["username"] = $queryResult['username'];

                // Set the auth cookie with their user ID, and set the timeout for 7 days from now
                setcookie("jhplumbingauth", $queryResult['id'], time() + (86400 * 7));

                echo $_SESSION["username"];
            } else
            {
                echo "Invalid Login Credentials";
                exit;
            }
        } else
        {
            echo "Internal Database Error";
            exit;
        }
    }
    else
    {
        if (session_id() == null) session_start();

        $_SESSION['userid'] = $_COOKIE['jhplumbingauth'];
    }
?>